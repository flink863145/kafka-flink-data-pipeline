package src.main.java.org.example;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Properties;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {

    /*
    To consume data from Kafka with Flink we need to provide a topic and a Kafka address. We should also provide a groupid
    which will be used to hold offsets so we won’t always read the whole data from the beginning.
    Let’s create a static method that will make the creation of FlinkKafkaConsumer easier:
    This method takes a topic, kafkaAddress, and kafkaGroup and creates the FlinkKafkaConsumer that will consume data from given topic as a String since we have used SimpleStringSchema to decode data.
    The number 011 in the name of class refers to the Kafka version.
     */

    public static FlinkKafkaConsumer<String> createStringConsumerForTopic(
            String topic, String kafkaAddress, String kafkaGroup ) {

        Properties props = new Properties();
        props.setProperty("bootstrap.servers", kafkaAddress);
        props.setProperty("group.id",kafkaGroup);
        FlinkKafkaConsumer<String> consumer = new FlinkKafkaConsumer<>(
                topic, new SimpleStringSchema(), props);

        return consumer;
    }


    /*
    To produce data to Kafka, we need to provide Kafka address and topic that we want to use. Again, we can create
    a static method that will help us to create producers for different topics:
    This method takes only topic and kafkaAddress as arguments since there’s no need to provide group id when we are
    producing to Kafka topic.
     */

    public static FlinkKafkaProducer<String> createStringProducer(
            String topic, String kafkaAddress){

        return new FlinkKafkaProducer<>(kafkaAddress,
                topic, new SimpleStringSchema());
    }


    /*
    After creating the function, we can use it in stream processing:
    The application will read data from the flink_input topic, perform operations on the stream and then save the
    results to the flink_output topic in Kafka.
     */
    public static void capitalize() throws Exception {
        String inputTopic = "flink_input";
        String outputTopic = "flink_output";
        String consumerGroup = "baeldung";
        String address = "localhost:9092";
        final StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();

        FlinkKafkaConsumer<String> flinkKafkaConsumer = createStringConsumerForTopic(
                inputTopic, address, consumerGroup);
        DataStream<String> stringInputStream = environment
                .addSource(flinkKafkaConsumer);

        FlinkKafkaProducer<String> flinkKafkaProducer = createStringProducer(
                outputTopic, address);

        stringInputStream
                .map(new WordsCapitalizer())
                .addSink(flinkKafkaProducer);
        environment.execute("Kafka Example");
    }


    public static void main(String[] args) throws Exception {
        capitalize();
        // https://www.baeldung.com/java-json-file-data-kafka-topic
        // https://www.baeldung.com/kafka-flink-data-pipeline
        // transform a class to json

    }


    }
