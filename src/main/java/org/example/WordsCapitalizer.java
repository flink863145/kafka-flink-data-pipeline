package src.main.java.org.example;

import org.apache.flink.api.common.functions.MapFunction;

    /*
    When we have a fully working consumer and producer, we can try to process data from Kafka and then save our results back to Kafka.
    n this example, we’re going to capitalize words in each Kafka entry and then write it back to Kafka.
    For this purpose we need to create a custom MapFunction:
     */

public class WordsCapitalizer implements MapFunction<String, String> {
    public String map(String s) {
        System.out.println("Hellloooo ");
        System.out.println("s.toUpperCase() ==> " + s.toUpperCase());
        return s.toUpperCase();
    }
}